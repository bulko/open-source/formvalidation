<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Test JS validation</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
	<style type='text/css'>
		#button {
			border: 1px solid grey;
			display: block;
			width: 50px;
			height: 50px;
		}
	</style>
	<form action="">
		<input type="hidden" name="origine" value="">
		<input type="hidden" name="optin_1" id="optin1" value="N">

		<input type="radio" required data-validationtype='civ' id="civ1" name="civilite" value="MLLE">
		<label for="civ1" style="font-weight:normal;" class="labelciv">Srta.</label>
		<input type="radio" required data-validationtype='civ' id="civ2" name="civilite" value="MME">
		<label for="civ2" style="font-weight:normal;" class="labelciv">Sra.</label>
		<input type="radio" required data-validationtype='civ' id="civ3" name="civilite" value="M">
		<label for="civ3" style="font-weight:normal;" class="labelciv">Sr.</label>

		<input required data-validationtype='prenom' type="text" name="prenom" class="form-control input-form1" id="inputPrenom" placeholder="*Nombre:" value="">

		<input required data-validationtype='nom' type="text" name="nom" class="form-control input-form1" id="inputNom" placeholder="*Apellidos:" value="">

		<input required data-validationtype='email' type="email" name="email" class="form-control input-form1" id="inputEmail" placeholder="*Email:" value="">

		<select required data-validationtype='DOB' class="form-control" name="jour_de_naissance" id="inputDOB1">
			<option value="">DD</option>
			<?php
    			for ($i=1; $i < 32; $i++) {
        			echo '<option value="'.$i.'"';
        			echo '>'.$i.'</option>';
    			}
			?>
		</select>

		<select required data-validationtype='DOB' class="form-control" name="mois_de_naissance" id="inputDOB2">
			<option value="">MM</option>
			<?php
    			for ($i=1; $i < 13; $i++) {
        			echo '<option value="'.$i.'"';
        			echo '>'.$i.'</option>';
    			}
			?>
		</select>

		<select required data-validationtype='DOB' class="form-control" name="annee_de_naissance" id="inputDOB3">
			<option value="">AAAA</option>
			<?php
    			for ($i=1997; $i > 1914; $i--) {
        			echo '<option value="'.$i.'"';
        			echo '>'.$i.'</option>';
    			}
			?>
		</select>
		<input required data-validationtype='CP' type="num" maxlength="5" name="code_postal" class="form-control" id="inputCP" placeholder="*Código postal:" value="<?php if(isset($joueur['code_postal']) && !empty($joueur['code_postal'])) { echo urldecode(str_pad($joueur['code_postal'], 5, " 0 ", STR_PAD_LEFT)); } ?>">

		<select required data-validationtype='pays' name="pays" id="inputPays" class="form-control">
			<option value="ES">España</option>
			<option value="OP">Otro país</option>
		</select>

		<input type="tel" required data-validationtype='tel' class="form-control" maxlength="10" name="telephone1" id="inputTel" placeholder='*Tu teléfono' value="">

        <p><input type="checkbox" required data-validationtype='check' id="check2"><label for="check2"> Obliger de checker cette boite.</label></p>

		<div id='button'>valider</div>
	</form>
	<footer>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.3.2/notify.js"></script>
		<script src="ErrorMessages.js"></script>
		<script src="FormValidation.js"></script>
		<script src="script.js"></script>
	</footer>
</body>

</html>
