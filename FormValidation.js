	//Config variable
	var lang = 'pl';
	var notify = true;

	//init variable
	var error = 0;

	//---------User called methods-------------//
	function globalValidation() {
		//reload error variable at each call
		error = 0;
		//each input is tested
		$('input, select').each(function (index) {
			//we don't select hidden input or unrequired input
			if ($(this).attr('type') != 'hidden' && $(this).attr('required') && error == 0) {
				validationType = $(this).data('validationtype');
				switch (validationType) {
				case 'tel':
					//validation rule
					var telephone = $(this).val();
					if (telephone == '') {
						displayError(errorMessage[lang][0].tel_empty, $(this));
					} else if (telephone.length != 9 || isNaN(telephone) || (telephone.charAt(0) != 5 && telephone.charAt(0) != 6 && telephone.charAt(0) != 7 && telephone.charAt(0) != 8)) {
						displayError(errorMessage[lang][0].tel, $(this));
					}
					break;
				case 'civ':
					//validation rule
					if ($("input[name='civilite']:checked").length <= 0) {
						displayError(errorMessage[lang][0].civ_empty, $(this));
					}
					break;
				case 'email':
					//validation rule
					var email = $(this).val();
					if (email == '') {
						displayError(errorMessage[lang][0].email_empty, $(this));
					} else if (!IsEmail(email)) {
						displayError(errorMessage[lang][0].email, $(this));
					}
					break;
				case 'nom':
					//validation rule
					var nom = $(this).val();
					if (nom == '') {
						displayError(errorMessage[lang][0].nom_empty, $(this));
					} else if (!syntax_char(nom)) {
						displayError(errorMessage[lang][0].nom, $(this));
					}
					break;
				case 'prenom':
					//validation rule
					var prenom = $(this).val();
					if (prenom == '') {
						displayError(errorMessage[lang][0].prenom_empty, $(this));
					} else if (!syntax_char(prenom)) {
						displayError(errorMessage[lang][0].prenom, $(this));
					}
					break;
				case 'CP':
					//validation rule
					var CP = $(this).val();
					if (CP == '') {
						displayError(errorMessage[lang][0].CP_empty, $(this));
					} else if (CP.length < 6 || isNaN(CP.substr(0, 2))) {
						displayError(errorMessage[lang][0].CP, $(this));
					}
					break;
				case 'pays':
					//validation rule
					var pays = $(this).val();
					if (pays == '') {
						displayError(errorMessage[lang][0].pays_empty, $(this));
					}
					break;
				case 'check':
					//validation rule
					if (!$(this).is(':checked')) {
						displayError(errorMessage[lang][0].default, $(this));
					}
					break;
				case 'DOB':
					//validation rule
					var DOB = $(this).val();
					var Y = $('select[name="annee_de_naissance"]').val();
					var fullDate = new Date().getFullYear();
					if (DOB == '') {
						displayError(errorMessage[lang][0].DOB_empty, $(this));
					} else if (fullDate - Y < 18 ) {
						displayError(errorMessage[lang][0].DOB, $(this));
					}
					break;
					//------add your special input type here----
					//				case 'your data-validationType':
					//					your rule
					//				break;
				default:
					if ($(this).val() == '') {
						displayError(errorMessage[lang][0].default, $(this));
					}
					break;
				}
			}
		});
		if (error == 1) {
			return false;
		} else {
			return true;
		}
	};

	//----------REGEXP method-----------//

	function syntax_char(nom) {
		var reg_alphab = /^[a-zA-ZÀÂÇÈÉÊËÎÔÙÛàâçèéêëîôùûìèùìÄÈÊËÎÏöòÔÓÖÙÛÇÜáÁéÉíÍóÓúÚñÑĄąĘęÓóĆćŁłŃńŚśŹźŻż\"\-\ ]+$/;
		if (!(reg_alphab.exec(nom) != null)) {
			return false;
		} else {
			return true
		}
	}

	function syntax_char_int(nom) {
		var reg_alphab = /^[a-zA-Z0-9ÀÂÇÈÉÊËÎÔÙÛàâçèéêëîôùûìèùìÄÈÊËÎÏöòÔÓÖÙÛÇÜáÁéÉíÍóÓúÚñÑĄąĘęÓóĆćŁłŃńŚśŹźŻż,'\"\-\ ]+$/;
		if (!(reg_alphab.exec(nom) != null)) {
			return false;
		} else {
			return true
		}
	}

	function IsEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}

	function displayError(errortxt, context) {
		error = 1;
		if (notify == true) {
			context.notify(errortxt);
		} else {
			alert(errortxt);
		}
	}
